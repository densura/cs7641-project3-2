from sklearn.tree import DecisionTreeClassifier

import rfe_util
from segmentation_dataset import load_train_test_data

N_COMPONENTS = 12


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    rfe_util.evaluate_rfe(X_train, y_train, estimator=DecisionTreeClassifier(random_state=0),
                          n_features_list=range(1, 18),
                          file_prefix="segmentation")

    rfe_util.run_ref(X_train, y_train, estimator=DecisionTreeClassifier(random_state=0), file_prefix="segmentation",
                     n_features=17)


if __name__ == "__main__":
    main()
