from sklearn import random_projection
from sklearn.decomposition import TruncatedSVD, FastICA, PCA
from sklearn.feature_selection import RFE
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier

from curve import show_learning_curve
from segmentation_dataset import load_train_test_data
from util import calculate_f1_score


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    pca_n_components = 12  # OK
    ica_n_components = 12  # OK
    rp_n_components = 15  # OK
    tsvd_n_components = 12  # OK
    rfe_n_features_to_select = 17  # OK
    n_clusters = 9

    pca = PCA(random_state=0, n_components=pca_n_components)
    ica = FastICA(
        random_state=0, n_components=ica_n_components, whiten=True, max_iter=2000
    )
    grp = random_projection.GaussianRandomProjection(
        random_state=0, n_components=rp_n_components
    )
    tsvd = TruncatedSVD(random_state=0, n_components=tsvd_n_components)
    rfe = RFE(DecisionTreeClassifier(random_state=0), n_features_to_select=rfe_n_features_to_select, step=1)
    rfe.fit(X, y)

    pca_X_train = pca.fit_transform(X_train)
    ica_X_train = ica.fit_transform(X_train)
    rp_X_train = grp.fit_transform(X_train)
    # tsvd_X_train = tsvd.fit_transform(X_train)
    rfe_X_train = rfe.transform(X_train)

    pca_clf = MLPClassifier(max_iter=10000)
    _ = pca_clf.fit(pca_X_train, y_train.values.ravel())

    ica_clf = MLPClassifier(max_iter=10000)
    _ = ica_clf.fit(ica_X_train, y_train.values.ravel())

    rp_clf = MLPClassifier(max_iter=10000)
    _ = rp_clf.fit(rp_X_train, y_train.values.ravel())

    rfe_clf = MLPClassifier(max_iter=10000)
    _ = rfe_clf.fit(rfe_X_train, y_train.values.ravel())

    import numpy as np

    # PCA
    show_learning_curve(
        MLPClassifier(),
        pca_X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (PCA)",
        ylabel="f1_macro_score",
        file_prefix="segmentation_pca",
        scoring="f1_macro",
    )

    print("\n== Base Training Score and Confusion Matrix (PCA) ==")
    calculate_f1_score(pca_clf, pca_X_train, y_train.values.ravel(), average="macro")
    print("\n== Base Test Score and Confusion Matrix (PCA) ==")
    calculate_f1_score(pca_clf, pca.transform(X_test), y_test.values.ravel(), average="macro")

    # ICA
    show_learning_curve(
        MLPClassifier(),
        ica_X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (ICA)",
        ylabel="f1_macro_score",
        file_prefix="segmentation_ica",
        scoring="f1_macro",
    )

    print("\n== Base Training Score and Confusion Matrix (ICA) ==")
    calculate_f1_score(ica_clf, ica_X_train, y_train.values.ravel(), average="macro")
    print("\n== Base Test Score and Confusion Matrix (ICA) ==")
    calculate_f1_score(ica_clf, ica.transform(X_test), y_test.values.ravel(), average="macro")

    # Randomized Projection
    show_learning_curve(
        MLPClassifier(),
        rp_X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (RP)",
        ylabel="f1_macro_score",
        file_prefix="segmentation_rp",
        scoring="f1_macro",
    )
    print("\n== Base Training Score and Confusion Matrix (RP) ==")
    calculate_f1_score(rp_clf, rp_X_train, y_train.values.ravel(), average="macro")
    print("\n== Base Test Score and Confusion Matrix (RP) ==")
    calculate_f1_score(rp_clf, grp.transform(X_test), y_test.values.ravel(), average="macro")

    # RFE
    show_learning_curve(
        MLPClassifier(),
        rfe_X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (RFE)",
        ylabel="f1_macro_score",
        file_prefix="segmentation_rfe",
        scoring="f1_macro",
    )
    print("\n== Base Training Score and Confusion Matrix (RFE) ==")
    calculate_f1_score(rfe_clf, rfe_X_train, y_train.values.ravel(), average="macro")
    print("\n== Base Test Score and Confusion Matrix (RFE) ==")
    calculate_f1_score(rfe_clf, rfe.transform(X_test), y_test.values.ravel(), average="macro")

    # Truncated SVD
    # show_learning_curve(
    #     MLPClassifier(),
    #     rp_X_train,
    #     y_train.values.ravel(),
    #     np.linspace(0.1, 1.0, 10),
    #     "Neural Network Learning Curve (TSVD)",
    #     ylabel="f1_macro_score",
    #     file_prefix="segmentation_tsvd",
    #     scoring="f1_macro",
    # )
    # print("\n== Base Training Score and Confusion Matrix (TSVD) ==")
    # calculate_f1_score(tsvd_clf, tsvd_X_train, y_train.values.ravel(), average="macro")


if __name__ == "__main__":
    main()
