from funcs import rp_util
from adult_dataset import load_train_test_data

N_COMPONENTS_LIST = range(2, 15)


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data()
    rp_util.rp_choose_components(X, N_COMPONENTS_LIST, "adult")
    rp_util.rp_calculate_variances(X, N_COMPONENTS_LIST, 20, "adult")


if __name__ == "__main__":
    main()
