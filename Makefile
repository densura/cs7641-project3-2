NOW := $(shell date "+%Y-%m-%d_%H:%M:%S")

kmeans_segmentation:
	python3 kmeans_segmentation.py

kmeans_adult:
	python3 kmeans_adult.py

em_segmentation:
	python3 em_segmentation.py

em_adult:
	python3 em_adult.py

pca_segmentation:
	python3 pca_segmentation.py

pca_adult:
	python3 pca_adult.py

ica_segmentation:
	python3 ica_segmentation.py

ica_adult:
	python3 ica_adult.py

rp_segmentation:
	python3 rp_segmentation.py

rp_adult:
	python3 rp_adult.py

rfe_segmentation:
	python3 rfe_segmentation.py

rfe_adult:
	python3 rfe_adult.py

kmeans_with_dr_segmentation:
	python3 kmeans_with_dr_segmentation.py

em_with_dr_segmentation:
	python3 em_with_dr_segmentation.py

kmeans_with_dr_adult:
	python3 kmeans_with_dr_adult.py

em_with_dr_adult:
	python3 em_with_dr_adult.py

nn_dr:
	python3 nn_dr.py

nn_clustering:
	python3 nn_clustering.py