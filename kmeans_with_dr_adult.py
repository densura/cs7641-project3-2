from datetime import datetime

import pandas as pd
from sklearn import random_projection
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA, FastICA, TruncatedSVD
from sklearn.feature_selection import RFE
from sklearn.tree import DecisionTreeClassifier
from yellowbrick.cluster import KElbowVisualizer

from adult_dataset import load_train_test_data

def elbow(X, file_prefix):
    model = KMeans()
    visualizer = KElbowVisualizer(model, k=(2, 14))
    visualizer.fit(X)  # Fit the data to the visualizer
    visualizer.show(
        outpath="plots/{}_kmeans-with-dr-elbow-{}.png".format(file_prefix, datetime.now()),
        clear_figure=True
    )

def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    pca_n_components = 7  # OK
    ica_n_components = 12  # OK
    rp_n_components = 12  # OK

    rfe_n_features_to_select = 11

    pca_n_clusters = 5  # OK
    ica_n_clusters = 7  # OK
    rp_n_clusters = 4  # OK
    rfe_n_clusters = 5  # OK

    pca = PCA(random_state=0, n_components=pca_n_components)
    ica = FastICA(
        random_state=0, n_components=ica_n_components, whiten=True, max_iter=2000
    )
    grp = random_projection.GaussianRandomProjection(
        random_state=0, n_components=rp_n_components
    )
    # tsvd = TruncatedSVD(random_state=0, n_components=tsvd_n_components)
    rfe = RFE(DecisionTreeClassifier(random_state=0), n_features_to_select=rfe_n_features_to_select, step=1)
    rfe.fit(X, y)

    pca_Xt = pca.fit_transform(X)
    ica_Xt = ica.fit_transform(X)
    rp_Xt = grp.fit_transform(X)
    rfe_Xt = rfe.transform(X)

    elbow(pca_Xt, "pca_adult")
    elbow(ica_Xt, "ica_adult")
    elbow(rp_Xt, "rp_adult")
    elbow(rfe_Xt, "rfe_adult")

    predict_pairplot(
        pca_Xt,
        "IMAGE-CLASS",
        pca_n_clusters,
        "plots/adult-pca-kmeans-predict-pairplot-all",
        var_list=[0,1,2]
    )
    predict_pairplot(
        ica_Xt,
        "IMAGE-CLASS",
        ica_n_clusters,
        "plots/adult-ica-kmeans-predict-pairplot-all",
        var_list=[0, 1, 2]
    )
    predict_pairplot(
        rp_Xt,
        "IMAGE-CLASS",
        rp_n_clusters,
        "plots/adult-rp-kmeans-predict-pairplot-all",
        var_list=[0, 1, 2]
    )
    predict_pairplot(
        rfe_Xt,
        "IMAGE-CLASS",
        rfe_n_clusters,
        "plots/adult-rfe-kmeans-predict-pairplot-all",
        var_list=[0, 1, 2]
    )


def predict_pairplot(X, hue, n_clusters, file_name, var_list=None):
    import seaborn as sns
    import matplotlib.pyplot as plt

    model = KMeans(n_clusters=n_clusters, random_state=0)
    model.fit(X)
    y_predict = model.predict(X)
    df = pd.concat([pd.DataFrame(X), pd.DataFrame(y_predict, columns=[hue])], axis=1)
    sns.pairplot(
        df,
        hue=hue,
        y_vars=var_list,
        x_vars=var_list,
        palette=sns.color_palette(n_colors=n_clusters),
        plot_kws={"s": 1},
    )
    plt.savefig("{}_{}.png".format(file_name, datetime.now()), format="png")


if __name__ == "__main__":
    main()
