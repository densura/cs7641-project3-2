import csv
from datetime import datetime

import matplotlib.pyplot as plt
import seaborn as sns
from kmodes.kprototypes import KPrototypes
from sklearn import metrics
from yellowbrick.cluster import KElbowVisualizer

import curve
from adult_dataset import load_train_test_data

CAT_COLUMNS = [5, 6, 7, 8, 9, 10, 11, 12]


def elbow(X):
    model = KPrototypes(random_state=0, verbose=1, n_jobs=4)
    visualizer = KElbowVisualizer(model, k=(2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15))

    print("== Running fit")
    visualizer.fit(X, categorical=CAT_COLUMNS)
    visualizer.show(outpath="plots/kmeans-adult-elbow-{}.png".format(datetime.now()))

    print("== cluster_centroids_")
    print(model.cluster_centroids_)

    print("== cost_")
    print(model.cost_)

    print("== n_iter_")
    print(model.n_iter_)


def pairplot_all(X, n_clusters):
    model = KPrototypes(random_state=0, n_clusters=n_clusters, verbose=1, n_jobs=4)

    print("== Running fit")
    model.fit(X, categorical=CAT_COLUMNS)
    y_predict = model.predict(X, categorical=CAT_COLUMNS)

    df = X
    df["CLUSTER-LABEL"] = y_predict
    sns.pairplot(
        df,
        hue="CLUSTER-LABEL",
        palette=sns.color_palette(n_colors=n_clusters),
        plot_kws={"s": 1},
    )
    plt.savefig(
        "plots/kmeans-adult-pairplot-all-{}.png".format(datetime.now()), format="png"
    )


def pairplot_select(X, y_labels, n_clusters):
    model = KPrototypes(random_state=0, n_clusters=n_clusters, verbose=1, n_jobs=4)

    print("== Running fit")
    model.fit(X, categorical=CAT_COLUMNS)
    y_predict = model.predict(X, categorical=CAT_COLUMNS)

    df = X
    df["CLUSTER-LABEL"] = y_predict
    # var_list = ["capital-gain", "age", "marital-status"]
    var_list = ["capital-gain", "education-num"]
    sns.pairplot(
        df,
        hue="CLUSTER-LABEL",
        y_vars=var_list,
        x_vars=var_list,
        palette=sns.color_palette(n_colors=n_clusters),
        plot_kws={"s": 1},
    )
    plt.savefig(
        "plots/kmeans-adult-pairplot-select-{}.png".format(datetime.now()), format="png"
    )

    rand_score = metrics.rand_score(y_predict, y_labels['income'])
    rand_score_data_file_name = "plots/{}_{}.csv".format(
        "adult-kmeans-rand_score", datetime.now()
    )
    with open(rand_score_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerow(["rand_score", rand_score])


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    # elbow(X)

    n_clusters = 8
    pairplot_all(X, n_clusters)
    pairplot_select(X, y, n_clusters)

    # Ground Truth
    curve.gt_pairplot(
        df,
        hue='income',
        file_name='plots/adult-gt-pairplot-all',
        n_colors=2
    )
    curve.gt_pairplot(
        df,
        hue='income',
        var_list=["capital-gain", "age", "marital-status"],
        file_name='plots/adult-gt-pairplot-select',
        n_colors=2
    )


if __name__ == "__main__":
    main()
