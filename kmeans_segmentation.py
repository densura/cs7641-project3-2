import csv
from datetime import datetime

import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans
from yellowbrick.cluster import KElbowVisualizer

from segmentation_dataset import load_train_test_data

N_CLUSTERS_LIST = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]


def elbow(X):
    model = KMeans()
    visualizer = KElbowVisualizer(model, k=(2, 15))
    visualizer.fit(X)  # Fit the data to the visualizer
    visualizer.show(
        outpath="plots/kmeans-segmentation-elbow-{}.png".format(datetime.now())
    )


def predict_pairplot(X, y_labels, hue, var_list):
    from sklearn import metrics

    n_clusters = 7
    model = KMeans(n_clusters=n_clusters, random_state=0)
    model.fit(X)
    y_predict = model.predict(X)
    df = X
    df[hue] = y_predict
    sns.pairplot(
        df,
        hue=hue,
        y_vars=var_list,
        x_vars=var_list,
        palette=sns.color_palette(n_colors=n_clusters),
        plot_kws={"s": 1},
    )

    rand_score = metrics.rand_score(y_predict, y_labels['IMAGE-CLASS'])
    rand_score_data_file_name = "plots/{}_{}.csv".format(
        "segmentation-kmeans-rand_score", datetime.now()
    )
    with open(rand_score_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerow(["rand_score", rand_score])

    plt.savefig(
        "plots/segmentation-pairplot-{}.png".format(datetime.now()), format="png"
    )


def gt_pairplot(df, hue, var_list):
    sns.pairplot(
        df,
        hue=hue,
        y_vars=var_list,
        x_vars=var_list,
        palette=sns.color_palette(n_colors=7),
        plot_kws={"s": 1},
    )
    plt.savefig(
        "plots/segmentation-gt-pairplot-select-{}.png".format(datetime.now()),
        format="png",
    )


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)

    var_list = ["RAWRED-MEAN", "RAWBLUE-MEAN", "RAWGREEN-MEAN"]
    elbow(X)
    predict_pairplot(X, y, "IMAGE-CLASS", var_list)
    predict_pairplot(X, y, "IMAGE-CLASS", None)
    gt_pairplot(df, 'IMAGE-CLASS', var_list)


if __name__ == "__main__":
    main()
