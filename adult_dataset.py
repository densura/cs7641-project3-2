import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder, StandardScaler, OrdinalEncoder
from sklearn.model_selection import train_test_split

COLUMN_NAMES = {
    0: "age",
    1: "workclass",
    2: "fnlwgt",
    3: "education",
    4: "education-num",
    5: "marital-status",
    6: "occupation",
    7: "relationship",
    8: "race",
    9: "sex",
    10: "capital-gain",
    11: "capital-loss",
    12: "hours-per-week",
    13: "native-country",
    14: "income",
}

NUM_FEATURES = [
    "age",
    "education-num",
    "capital-gain",
    "capital-loss",
    "hours-per-week",
]
CAT_FEATURES = [
    "workclass",
    "marital-status",
    "occupation",
    "relationship",
    "race",
    "sex",
    "native-country",
    "education",
]
TEST_SET_SIZE = 0.1


def load_train_test_data(small=False):
    adult_data_df = pd.read_csv("./dataset/adult.data", header=None)
    adult_data_df = cleanup(adult_data_df.rename(COLUMN_NAMES, axis=1))
    X, y, df = encode_data(adult_data_df)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=TEST_SET_SIZE, random_state=10, stratify=y
    )

    if small:
        return X_train[:100], X_test[:100], y_train[:100], y_test[:100], X, y, df
    else:
        return X_train, X_test, y_train, y_test, X, y, df


def encode_data(df):
    adult_num_df = df[NUM_FEATURES]
    adult_cat_df = df[CAT_FEATURES]
    ordinal_encoder = OrdinalEncoder()
    one_host_encoder = OneHotEncoder()
    adult_cat_encoded_matrix = one_host_encoder.fit_transform(adult_cat_df)
    adult_cat_ordinal_encoded_matrix = ordinal_encoder.fit_transform(adult_cat_df)

    # adult_cat_encoded_df = pd.DataFrame(adult_cat_encoded_matrix.toarray())
    adult_cat_ordinal_encoded_df = pd.DataFrame(adult_cat_ordinal_encoded_matrix)

    # Apply scaling to numerical features
    scaler = StandardScaler()
    adult_num_df = pd.DataFrame(
        scaler.fit_transform(adult_num_df), columns=adult_num_df.columns
    )
    dfs = []
    adult_cat_ordinal_renamed_df = adult_cat_ordinal_encoded_df.rename(
        {
            0: "workclass",
            1: "marital-status",
            2: "occupation",
            3: "relationship",
            4: "race",
            5: "sex",
            6: "native-country",
            7: "education",
        },
        axis=1,
    )
    dfs = dfs + [
        adult_num_df.reset_index(drop=True),
        adult_cat_ordinal_renamed_df.reset_index(drop=True),
    ]

    X = pd.concat(dfs, axis=1)
    y = pd.DataFrame(
        df["income"].replace({"<=50K": -1, "<=50K.": -1, ">50K": 1, ">50K.": 1})
    ).reset_index(drop=True)
    return X, y, pd.concat(dfs + [y], axis=1).reset_index(drop=True)


def cleanup(df):
    df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    df = df.replace("?", np.nan)
    df = df.dropna()
    return df


if __name__ == "__main__":
    load_train_test_data()
