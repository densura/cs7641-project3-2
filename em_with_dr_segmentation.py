from datetime import datetime

import pandas as pd
from sklearn import random_projection, mixture
from sklearn.decomposition import PCA, FastICA, TruncatedSVD
from sklearn.feature_selection import RFE
from sklearn.tree import DecisionTreeClassifier

import curve
from segmentation_dataset import load_train_test_data


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    pca_n_components = 12  # OK
    ica_n_components = 12  # OK
    rp_n_components = 15  # OK
    rfe_n_features_to_select = 17
    n_clusters = 9  # OK

    pca_n_clusters = 15
    ica_n_clusters = 12
    rp_n_clusters = 15
    rfe_n_clusters = 8

    pca = PCA(random_state=0, n_components=pca_n_components)
    ica = FastICA(
        random_state=0, n_components=ica_n_components, whiten=True, max_iter=2000
    )
    grp = random_projection.GaussianRandomProjection(
        random_state=0, n_components=rp_n_components
    )

    pca_Xt = pca.fit_transform(X)
    ica_Xt = ica.fit_transform(X)
    rp_Xt = grp.fit_transform(X)
    rfe = RFE(DecisionTreeClassifier(random_state=0), n_features_to_select=rfe_n_features_to_select, step=1)
    rfe.fit(X, y)
    rfe_Xt = rfe.transform(X)

    components_range = range(2, 18)
    # curve.plot_gmm_bic(pca_Xt, components_range, "segmentation-pca")
    # curve.plot_gmm_bic(ica_Xt, components_range, "segmentation-ica")
    # curve.plot_gmm_bic(rp_Xt, components_range, "segmentation-rp")
    # curve.plot_gmm_bic(rfe_Xt, components_range, "segmentation-rfe")


    predict_pairplot(
        pca_Xt,
        "IMAGE-CLASS",
        pca_n_clusters,
        "plots/segmentation-pca-em-predict-pairplot-all",
        var_list=[0, 1, 2]
    )
    predict_pairplot(
        ica_Xt,
        "IMAGE-CLASS",
        ica_n_clusters,
        "plots/segmentation-ica-em-predict-pairplot-all",
        var_list=[0, 1, 2]
    )
    predict_pairplot(
        rp_Xt,
        "IMAGE-CLASS",
        rp_n_clusters,
        "plots/segmentation-rp-em-predict-pairplot-all",
        var_list=[0, 1, 2]
    )
    predict_pairplot(
        rfe_Xt,
        "IMAGE-CLASS",
        rfe_n_clusters,
        "plots/segmentation-rfe-em-predict-pairplot-all",
        var_list=[0, 1, 2]
    )


def predict_pairplot(X, hue, n_clusters, file_name, var_list=None):
    import seaborn as sns
    import matplotlib.pyplot as plt

    gmm = mixture.GaussianMixture(
        random_state=0, n_components=n_clusters, covariance_type="full"
    )
    gmm.fit(X)
    y_predict = gmm.predict(X)
    df = pd.concat([pd.DataFrame(X), pd.DataFrame(y_predict, columns=[hue])], axis=1)
    sns.pairplot(
        df,
        hue=hue,
        y_vars=var_list,
        x_vars=var_list,
        palette=sns.color_palette(n_colors=n_clusters),
        plot_kws={"s": 1},
    )
    plt.savefig("{}_{}.png".format(file_name, datetime.now()), format="png")


if __name__ == "__main__":
    main()
