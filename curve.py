import csv
import itertools

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.model_selection import learning_curve
from datetime import datetime

import constants

"""
Stolen from https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html#sphx-glr-auto-examples-model-selection-plot-learning-curve-py
"""


def plot_learning_curve(
        estimator,
        title,
        X,
        y,
        y_label="Score",
        scoring=None,
        axes=None,
        ylim=None,
        cv=None,
        n_jobs=None,
        train_sizes=np.linspace(0.1, 1.0, 5),
):
    """
    Generate 3 plots: the test and training learning curve, the training
    samples vs fit times curve, the fit times vs score curve.

    Parameters
    ----------
    estimator : estimator instance
        An estimator instance implementing `fit` and `predict` methods which
        will be cloned for each validation.

    title : str
        Title for the chart.

    X : array-like of shape (n_samples, n_features)
        Training vector, where ``n_samples`` is the number of samples and
        ``n_features`` is the number of features.

    y : array-like of shape (n_samples) or (n_samples, n_features)
        Target relative to ``X`` for classification or regression;
        None for unsupervised learning.

    axes : array-like of shape (3,), default=None
        Axes to use for plotting the curves.

    ylim : tuple of shape (2,), default=None
        Defines minimum and maximum y-values plotted, e.g. (ymin, ymax).

    cv : int, cross-validation generator or an iterable, default=None
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:

          - None, to use the default 5-fold cross-validation,
          - integer, to specify the number of folds.
          - :term:`CV splitter`,
          - An iterable yielding (train, test) splits as arrays of indices.

        For integer/None inputs, if ``y`` is binary or multiclass,
        :class:`StratifiedKFold` used. If the estimator is not a classifier
        or if ``y`` is neither binary nor multiclass, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validators that can be used here.

    n_jobs : int or None, default=None
        Number of jobs to run in parallel.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    train_sizes : array-like of shape (n_ticks,)
        Relative or absolute numbers of training examples that will be used to
        generate the learning curve. If the ``dtype`` is float, it is regarded
        as a fraction of the maximum size of the training set (that is
        determined by the selected validation method), i.e. it has to be within
        (0, 1]. Otherwise it is interpreted as absolute sizes of the training
        sets. Note that for classification the number of samples usually have
        to be big enough to contain at least one sample from each class.
        (default: np.linspace(0.1, 1.0, 5))
    """
    if axes is None:
        _, axes = plt.subplots(1, 3, figsize=(20, 5))

    axes[0].set_title(title)
    if ylim is not None:
        axes[0].set_ylim(*ylim)
    axes[0].set_xlabel("Training examples")
    axes[0].set_ylabel(y_label)

    train_sizes, train_scores, test_scores, fit_times, _ = learning_curve(
        estimator,
        X,
        y,
        scoring=scoring,
        cv=cv,
        n_jobs=n_jobs,
        train_sizes=train_sizes,
        return_times=True,
        verbose=1,
    )
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    fit_times_mean = np.mean(fit_times, axis=1)
    fit_times_std = np.std(fit_times, axis=1)

    print("\n== Scores from learning curves ==")
    print("Train scores mean: {}".format(train_scores_mean))
    print("Train scores std: {}".format(train_scores_std))
    print("Validation scores: {}".format(test_scores_mean))
    print("Validatin scores std: {}".format(test_scores_std))
    print("Fit times mean: {}".format(fit_times_mean))
    print("Fit times std: {}".format(fit_times_std))

    # Plot learning curve
    axes[0].grid()
    axes[0].fill_between(
        train_sizes,
        train_scores_mean - train_scores_std,
        train_scores_mean + train_scores_std,
        alpha=0.1,
        color="r",
    )
    axes[0].fill_between(
        train_sizes,
        test_scores_mean - test_scores_std,
        test_scores_mean + test_scores_std,
        alpha=0.1,
        color="g",
    )
    axes[0].plot(
        train_sizes, train_scores_mean, "o-", color="r", label="Training score"
    )
    axes[0].plot(
        train_sizes, test_scores_mean, "o-", color="g", label="Cross-validation score"
    )
    axes[0].legend(loc="best")

    # Plot n_samples vs fit_times
    axes[1].grid()
    axes[1].plot(train_sizes, fit_times_mean, "o-")
    axes[1].fill_between(
        train_sizes,
        fit_times_mean - fit_times_std,
        fit_times_mean + fit_times_std,
        alpha=0.1,
    )
    axes[1].set_xlabel("Training examples")
    axes[1].set_ylabel("fit_times")
    axes[1].set_title("Scalability of the model")

    # Plot fit_time vs score
    fit_time_argsort = fit_times_mean.argsort()
    fit_time_sorted = fit_times_mean[fit_time_argsort]
    test_scores_mean_sorted = test_scores_mean[fit_time_argsort]
    test_scores_std_sorted = test_scores_std[fit_time_argsort]
    axes[2].grid()
    axes[2].plot(fit_time_sorted, test_scores_mean_sorted, "o-")
    axes[2].fill_between(
        fit_time_sorted,
        test_scores_mean_sorted - test_scores_std_sorted,
        test_scores_mean_sorted + test_scores_std_sorted,
        alpha=0.1,
    )
    axes[2].set_xlabel("fit_times")
    axes[2].set_ylabel(y_label)
    axes[2].set_title("Performance of the model")

    return plt, train_sizes, train_scores, test_scores, fit_times


from sklearn.model_selection import validation_curve
from sklearn import tree, mixture


def plot_validation_curve(
        estimator,
        X,
        y,
        title,
        xlabel,
        ylabel,
        param_range,
        param_label_range,
        param_name,
        scoring,
        cv=5,
        verbose=0,
        n_jobs=-2,
        file_prefix="",
        save_file=True,
):
    train_scores, test_scores = validation_curve(
        estimator,
        X,
        y,
        param_name=param_name,
        param_range=param_range,
        scoring=scoring,
        n_jobs=n_jobs,
        cv=cv,
        verbose=verbose,
    )
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid()
    linewidth = 2
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    print("\n== Scores from validation curve ({}) ==".format(param_name))
    print("Train scores mean: {}".format(train_scores_mean))
    print("Train scores std: {}".format(train_scores_std))
    print("Validation scores: {}".format(test_scores_mean))
    print("Validatin scores std: {}".format(test_scores_std))

    plt.plot(
        param_label_range, train_scores_mean, "o-", color="r", label="Training score"
    )
    plt.fill_between(
        param_label_range,
        train_scores_mean - train_scores_std,
        train_scores_mean + train_scores_std,
        alpha=0.2,
        color="r",
    )
    plt.plot(
        param_label_range,
        test_scores_mean,
        "o-",
        color="g",
        label="Cross-validation score",
    )
    plt.fill_between(
        param_label_range,
        test_scores_mean - train_scores_std,
        test_scores_mean + train_scores_std,
        alpha=0.2,
        color="g",
    )

    plt.legend(loc="best")

    if save_file:
        plt.savefig(
            "plots/{}_{}_{}.png".format(file_prefix, title, datetime.now()),
            format="png",
        )
    else:
        plt.show()

    plt.close()


def show_learning_curve(
        clf,
        X,
        y,
        learning_curve_train_sizes,
        title="Learning Curve",
        cv=5,
        ylabel="score",
        scoring="f1",
        file_prefix="",
        save_file=True,
):
    plot, train_sizes, train_scores, test_scores, fit_times = plot_learning_curve(
        clf,
        title,
        X,
        y,
        ylabel,
        train_sizes=learning_curve_train_sizes,
        cv=cv,
        scoring=scoring,
        n_jobs=-2,
    )

    if save_file:
        plt.savefig(
            "plots/{}_{}_{}.png".format(file_prefix, title, datetime.now()),
            format="png",
        )
        plt.close()
    else:
        plt.show()


def plot_curve(
        x,
        y_list,  # 2-d array of y values, each inner array has to be the same size as x
        xlabel,
        ylabel,
        line_labels,
        color_lis,
        title,
        file_name,
        dir="plots",
        save_file=True,
):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid()

    for idx in range(len(y_list)):
        y = y_list[idx]
        label = line_labels[idx]
        color = color_lis[idx]
        plt.plot(x, y, "-", color=color, label=label, linewidth=0.5)
        plt.legend(loc="best")

    if save_file:
        file_path = "{}/{}_{}.png".format(dir, file_name, datetime.now())
        print(f"Saving {file_path}")
        plt.savefig(file_path, format="png")
    else:
        plt.show()

    plt.close()


def plot_clusters(X, labels, n_clusters, xi, yi, x_label="x-axis", y_label="y-axis"):
    for i in range(0, n_clusters):
        xs = X[labels == i]
        plt.scatter(xs[:][xi], xs[:][yi], color=constants.COLORS[i])

    plt.title(f"K={n_clusters}")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.grid()
    plt.show()


def plot_clusters_3d(
        X,
        labels,
        n_clusters,
        xi,
        yi,
        zi,
        x_label="x-axis",
        y_label="y-axis",
        z_label="z-axis",
):
    ax = plt.axes(projection="3d")

    for i in range(0, n_clusters):
        xs = X[labels == i]
        xdata = xs[:][xi]
        ydata = xs[:][yi]
        zdata = xs[:][zi]
        ax.scatter3D(xdata, ydata, zdata, c=constants.COLORS[i], s=1)

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_zlabel(z_label)
    plt.grid()
    plt.show()


def plot_gmm_bic(X, n_components_range, file_prefix):
    cv_types = ["spherical", "tied", "diag", "full"]
    bic = []
    aic = []
    for cv_type in cv_types:
        for n_components in n_components_range:
            # Fit a Gaussian mixture with EM
            gmm = mixture.GaussianMixture(
                random_state=0, n_components=n_components, covariance_type=cv_type
            )
            gmm.fit(X)
            bic.append(gmm.bic(X))
            aic.append(gmm.aic(X))

    bic = np.array(bic)
    aic = np.array(aic)
    color_iter = itertools.cycle(["navy", "turquoise", "cornflowerblue", "darkorange"])
    bars = []

    # Plot the BIC scores
    plt.figure(figsize=(8, 6))
    for i, (cv_type, color) in enumerate(zip(cv_types, color_iter)):
        xpos = np.array(n_components_range) + 0.2 * (i - 2)
        bars.append(
            plt.bar(
                xpos,
                bic[i * len(n_components_range): (i + 1) * len(n_components_range)],
                width=0.2,
                color=color,
            )
        )
    bic_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "em-bic-plot", datetime.now()
    )
    bic_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "em-bic-plot", datetime.now()
    )
    plt.xticks(n_components_range)
    plt.ylim([bic.min() * 1.01 - 0.01 * bic.max(), bic.max()])
    plt.title("BIC score per model")
    plt.xlabel("Number of components")
    plt.ylabel("BIC Score")
    plt.legend([b[0] for b in bars], cv_types)
    plt.savefig(bic_plot_file_name, format="png")

    with open(bic_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerow(bic)

    aic_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "em-aic-plot", datetime.now()
    )
    aic_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "em-aic-plot", datetime.now()
    )

    # Plot the AIC scores
    plt.cla()
    bars = []
    plt.figure(figsize=(8, 6))
    for i, (cv_type, color) in enumerate(zip(cv_types, color_iter)):
        xpos = np.array(n_components_range) + 0.2 * (i - 2)
        bars.append(
            plt.bar(
                xpos,
                bic[i * len(n_components_range): (i + 1) * len(n_components_range)],
                width=0.2,
                color=color,
            )
        )
    plt.xticks(n_components_range)
    plt.ylim([aic.min() * 1.01 - 0.01 * aic.max(), aic.max()])
    plt.title("AIC score per model")
    plt.xlabel("Number of components")
    plt.ylabel("AIC Score")
    plt.legend([b[0] for b in bars], cv_types)
    plt.savefig(aic_plot_file_name, format="png")

    with open(aic_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerow(aic)


def plot_cum_explained_var_ratio(X, file_name):
    pca = PCA(random_state=0).fit(X)
    cumsum = np.cumsum(pca.explained_variance_ratio_)
    print(cumsum)
    plt.plot(cumsum)
    plt.xlabel("Number of components")
    plt.ylabel("Cumulative explained variance")

    file_path = "{}_{}.png".format(file_name, datetime.now())
    print(f"Saving {file_path}")
    plt.savefig(file_path, format="png")


def gt_pairplot(df, hue, file_name, n_colors, var_list=None):
    sns.pairplot(
        df,
        hue=hue,
        y_vars=var_list,
        x_vars=var_list,
        palette=sns.color_palette(n_colors=n_colors),
        plot_kws={"s": 1},
    )
    plt.savefig("{}-{}.png".format(file_name, datetime.now()), format="png")
