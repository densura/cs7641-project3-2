from funcs import pca_util
from adult_dataset import load_train_test_data

N_COMPONENTS_LIST = range(2, 14)
N_COMPONENTS = 8  # 0.96 cumulative explained variance


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    pca_util.pca_choose_components(X, list(N_COMPONENTS_LIST), "adult")
    print(X.columns)
    pca_util.run_pca(X, N_COMPONENTS, "adult")  # 0.96


if __name__ == "__main__":
    main()
