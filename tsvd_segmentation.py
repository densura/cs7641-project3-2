from segmentation_dataset import load_train_test_data
from funcs.tsvd_util import tsvd_choose_components

N_COMPONENTS_LIST = range(2, 15)
N_COMPONENTS = 12


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    tsvd_choose_components(X, list(N_COMPONENTS_LIST), "segmentation")


if __name__ == "__main__":
    main()
