Account: dsuratna3
Name: Dennis Suratna
Code and dataset are located at: https://gitlab.com/densura/cs7641-project3-2.git

To clone the code and the dataset, run: `git clone https://gitlab.com/densura/cs7641-project3-2.git`

The code was written using python 3.9. To install the dependencies, run the follwoing command: `pip3 install -r requirements.txt`

`adult_dataset.py` and `segmentation_dataset.py` contain the code to prepare the datasets.

Please run the following make targets to run the program for each section. Refer to Makefile for details.

KMeans for segmentation dataset: `make kmeans_segmentation`
KMeans for adult dataset: `make kmeans_adult`
EM for segmentation dataset: `make em_segmentation`
EM for adult dataset: `make em_adult`
PCA for segmentation dataset: `make pca_segmentation`
PCA for adult dataset: `make pca_adult`
ICA for segmentation dataset: `make ica_segmentation`
ICA for adult dataset: `make ica_adult`
RP for segmentation dataset: `make rp_segmentation`
RP for adult dataset: `make rp_adult`
RFE for segmentation dataset: `make rfe_segmentation`
RFE for adult dataset: `make rfe_adult`
KMeans with dimensionality reduction for segmentation dataset: `make kmeans_with_dr_segmentation`
KMeans with dimensionality reduction for adult dataset: `make kmeans_with_dr_adult`
EM with dimensionality reduction for segmentation dataset: `make em_with_dr_segmentation`
EM with dimensionality reduction for adult dataset: `make em_with_dr_adult`
Neural Network with Dimensionality Reduction on segmentation dataset: `make nn_dr`
Neural Network with Clustering on adult dataset: `make nn_clustering`

The plots and data will be generated under the `plots` folder.

The income classification dataset is from: https://archive.ics.uci.edu/ml/datasets/adult
The image classification dataset is from: https://archive.ics.uci.edu/ml/datasets/image+segmentation
